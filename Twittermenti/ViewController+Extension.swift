//
//  ViewController+Extension.swift
//  Twittermenti
//
//  Created by Leah Joy Ylaya on 12/23/20.
//  Copyright © 2020 London App Brewery. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func parseConfig() -> Config {
        let url = Bundle.main.url(forResource: "Keys", withExtension: "plist")!
        let data = try! Data(contentsOf: url)
        let decoder = PropertyListDecoder()
        return try! decoder.decode(Config.self, from: data)
    }
}

struct Config: Decodable {
    let secretKey: String
    let apiKey: String
    
    private enum CodingKeys: String, CodingKey {
        case secretKey = "api_secret"
        case apiKey = "api_key"
    }
}
